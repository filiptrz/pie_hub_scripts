local OrionLib = loadstring(game:HttpGet(('https://raw.githubusercontent.com/shlexware/Orion/main/source')))()

local Window = OrionLib:MakeWindow({Name = "PIE HUB [BEAR*]", HidePremium = false, SaveConfig = true, ConfigFolder = "PIEHUB_BEAR"})

local AUTO = Window:MakeTab({
	Name = "Auto Farm",
	Icon = "rbxassetid://4483345998",
	PremiumOnly = false
})

AUTO:AddToggle({
	
    qd = 0,
    
    Name = "Auto Quidz",
	Default = false,
	Callback = function(Value)
		if Value == true then
           qd = 0
           while wait(1) do

            if qd == 0 then
              
              game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game.Workspace.Obby.WinPart.CFrame

              end

           end
           else
           qd = 1
        end
	end    
})

AUTO:AddToggle({
	
    qd = 0,
    
    Name = "Auto Lobby",
	Default = false,
	Callback = function(Value)
		if Value == true then
           qd = 0
           while wait(5) do

            if qd == 0 then
              
              game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = game.Workspace.Obby.Spawn.CFrame

              end

           end
           else
           qd = 1
        end
	end    
})
